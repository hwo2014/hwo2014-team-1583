using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

public class Bot {
	public static void Main(string[] args) {
        if (args.Length != 4) {
            Console.WriteLine("usage: " + System.AppDomain.CurrentDomain.FriendlyName + " <host> <port> <botname> <botkey>");
            return;
        }

	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			Bot bot = new Bot(reader, writer, new Join(botName, botKey));
			bot.Run();
		}
	}

	private StreamReader reader;
	private StreamWriter writer;
	private Join join;

	Bot(StreamReader reader, StreamWriter writer, Join join) {
		this.reader = reader;
		this.writer = writer;
		this.join = join;
	}

	private void Send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}

	public void Run() {
		Send(join);

		string line;
		while((line = this.reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			switch(msg.msgType) {
			case "carPositions":
				OnCarPositions((JArray)msg.data);
				break;
			case "join":
				OnJoin((JObject)msg.data);
				break;
			case "yourCar":
				OnYourCar((JObject)msg.data);
				break;
			case "gameInit":
				OnGameInit((JObject)msg.data);
				break;
			case "gameStart":
				OnGameStart();
				break;
			case "gameEnd":
				OnGameEnd((JObject)msg.data);
				break;
			case "tournamentEnd":
				OnTournamentEnd();
				break;
			case "crash":
				OnCrash((JObject)msg.data);
				break;
			case "spawn":
				OnSpawn((JObject)msg.data);
				break;
			case "lapFinished":
				OnLapFinished((JObject)msg.data);
				break;
			case "dnf":
				OnDisqualified((JObject)msg.data);
				break;
			case "finish":
				OnFinish((JObject)msg.data);
				break;
			case "error":
				OnError((JObject)msg.data);
				break;
			default:
				Console.WriteLine("Unknown msg : " + msg.msgType);
				Send(new Ping());
				break;
			}
		}
	}
	
	private void OnJoin(JObject data) {
		Console.WriteLine("Joined");

		// Nothing to do.

		Send(new Ping());
	}

	private CarManager.Car.CarId myCarId;
	private void OnYourCar(JObject data) {
		Console.WriteLine("YourCar");

		myCarId = new CarManager.Car.CarId (data);

		Send(new Ping());
	}
	
	public static TrackInfo Track;
	private CarManager carManager;
	public static long totalLaps;
	public SpeedManager speedManager;

	private void OnGameInit(JObject data) {
		Console.WriteLine("GameInit");

		Track = new TrackInfo ((JObject)data["race"]["track"]);
		carManager = new CarManager ((JArray)data ["race"]["cars"]);
		JObject RaceSession = (JObject)data ["race"]["raceSession"];
		totalLaps = (long)RaceSession ["laps"];
		speedManager = new SpeedManager ();

		Send(new Ping());
	}
	
	private void OnGameStart() {
		Console.WriteLine("GameStart");

		Send(new Ping());
	}

	public static double resistanceConstant = 0.1;
	public static double massConstant = 5.0;
	//public static double wheelFriction = 0.321;
	public static double wheelFriction = 0.455;

	private void OnCarPositions(JArray data) {
		Console.WriteLine("CarPositions");
		Console.WriteLine(data);

		carManager.Update (data);

		CarManager.Car myCar = carManager.Cars [myCarId];
		Console.WriteLine("Speed : " + myCar.Speed);
		Console.WriteLine("Accel : " + myCar.Accel);
		double speed = myCar.Speed;
		double accel = myCar.Accel;

		double targetSpeed = double.MaxValue;

		Console.WriteLine("Type : " + Track.Pieces [myCar.PieceIndex].Type);

		targetSpeed = speedManager.MaxSpeed (myCar.StartLaneIndex, myCar.Lap, myCar.PieceIndex, myCar.InPieceDistance);

		Console.WriteLine("TargetSpeed : " + targetSpeed);

		double nextAccel = targetSpeed - speed;
		double nextForce = nextAccel * massConstant;
		nextForce += targetSpeed * resistanceConstant;

		if (Math.Abs (myCar.Angle + (myCar.AngleSpeed * 5.0)) >= 60.0) {
			nextForce = double.MinValue;
			Console.WriteLine ("Break");
		}

		Console.WriteLine("Throttle : " + nextForce);
		//Console.WriteLine("" + speed + "\t" + myCar.Angle + "\t" + Track.Pieces [myCar.PieceIndex].Type + "\t" + Track.Pieces [myCar.PieceIndex].GetRadius(myCar.StartLaneIndex));
		Send (new Throttle (nextForce));
		return;
	}

	private void OnGameEnd(JObject data) {
		Console.WriteLine("GameEnd");
		Send(new Ping());
	}

	private void OnTournamentEnd() {
		Console.WriteLine("TournamentEnd");
		Send(new Ping());
	}

	private void OnCrash(JObject data) {
		Console.WriteLine("Crash");
		Send(new Ping());
	}

	private void OnSpawn(JObject data) {
		Console.WriteLine("Spawn");
		Send(new Ping());
	}

	private void OnLapFinished(JObject data) {
		Console.WriteLine("LapFinished");
		Send(new Ping());
	}

	private void OnDisqualified(JObject data) {
		Console.WriteLine("Disqualified");
		Send(new Ping());
	}

	private void OnFinish(JObject data) {
		Console.WriteLine("Finish");
		Send(new Ping());
	}

	private void OnError(JObject data) {
		Console.WriteLine("Error");
		Console.WriteLine(data);
		Send(new Ping());
	}
}

public class SpeedManager
{
	public class PieceSpeed
	{
		public double[] endSpeed;

		public TrackInfo.Piece piece;
		public PieceSpeed(TrackInfo.Piece piece, PieceSpeed nextSpeed = null)
		{
			this.piece = piece;
			this.endSpeed = new double[Bot.Track.Lanes.Count];
			for (long i = 0; i < Bot.Track.Lanes.Count; i++)
			{
				if (nextSpeed == null)
					this.endSpeed[i] = double.MaxValue;
				else
					this.endSpeed[i] = nextSpeed.GetSpeed(i);
			}
		}

		public double GetSpeed(long laneIndex, double startDistance = 0.0)
		{
			double remainDistance = this.piece.GetLength (laneIndex) - startDistance;
			double speed = (((Bot.resistanceConstant / Bot.massConstant) * (1 - (Bot.resistanceConstant / Bot.massConstant))) * remainDistance) + endSpeed [laneIndex];

			return Math.Min(speed, piece.MaxSpeed (laneIndex));
		}
	}

	public PieceSpeed[] pieceSpeedList;
	public SpeedManager()
	{
		pieceSpeedList = new PieceSpeed[Bot.Track.Pieces.Length * Bot.totalLaps];

		long i = pieceSpeedList.Length;
		PieceSpeed nextSpeed = null;
		while (i-- > 0) {
			TrackInfo.Piece piece = Bot.Track.Pieces [i % Bot.Track.Pieces.Length];

			pieceSpeedList [i] = new PieceSpeed (piece, nextSpeed);
			nextSpeed = pieceSpeedList [i];
		}
	}

	public double MaxSpeed(long lane, long lap, long piece, double distance)
	{
		PieceSpeed pieceSpeed = pieceSpeedList[(lap * Bot.totalLaps) + piece];

		return pieceSpeed.GetSpeed (lane, distance);
	}
}

public class CarManager
{
	public class Car
	{
		public class CarId
		{
			public string Name { get; private set; }
			public string Color { get; private set; }

			public CarId(JObject Data)
			{
				this.Name = (string)Data["name"];
				this.Color = (string)Data["color"];
			}

			public CarId(string Name, string Color)
			{
				this.Name = Name;
				this.Color = Color;
			}

			public bool Equals(CarId OtherCar)
			{
				return this.Name == OtherCar.Name && this.Color == OtherCar.Color;
			}

			public override int GetHashCode ()
			{
				return this.Name.GetHashCode () * this.Color.GetHashCode();
			}

			public override bool Equals (object obj)
			{
				CarId otherCar = obj as CarId;
				if (otherCar == null)
					return false;

				return this.Name == otherCar.Name && this.Color == otherCar.Color;
			}

		}
		public CarId Id { get; private set; }

		public double Length { get; private set; }
		public double Width { get; private set; }
		public double GuideFlagPosition { get; private set; }

		public double Angle { get; private set; }
		public double AngleSpeed { get; private set; }
		public long PieceIndex { get; private set; }
		public double InPieceDistance { get; private set; }
		public long StartLaneIndex { get; private set; }
		public long EndLaneIndex { get; private set; }
		public long Lap { get; private set; }

		public double Speed { get; private set; }
		public double Accel { get; private set; }

		public Car(JObject Data)
		{
			Id = new CarId((JObject)Data["id"]);
			Length = (double)Data["dimensions"]["length"];
			Width = (double)Data["dimensions"]["width"];
			GuideFlagPosition = (double)Data["dimensions"]["guideFlagPosition"];
		}

		public void Update(JObject Data)
		{
			double Angle = (double)Data["angle"];
			long PieceIndex = (long)Data["piecePosition"]["pieceIndex"];
			double InPieceDistance = (double)Data["piecePosition"]["inPieceDistance"];
			long StartLaneIndex = (long)Data["piecePosition"]["lane"]["startLaneIndex"];
			long EndLaneIndex = (long)Data["piecePosition"]["lane"]["endLaneIndex"];
			long Lap = (long)Data["piecePosition"]["lap"];

			// Calc speed.
			double speed; 
			if (this.PieceIndex == PieceIndex)
				speed = InPieceDistance - this.InPieceDistance;
			else
				speed = InPieceDistance + (Bot.Track.Pieces[this.PieceIndex].GetLength(this.StartLaneIndex) - this.InPieceDistance);

			// Calc accel.
			double accel; 
			accel = speed - this.Speed;

			// Calc speed.
			double AngleSpeed = Angle - this.Angle;
			if (this.PieceIndex == PieceIndex)
				speed = InPieceDistance - this.InPieceDistance;
			else
				speed = InPieceDistance + (Bot.Track.Pieces[this.PieceIndex].GetLength(this.StartLaneIndex) - this.InPieceDistance);


			this.Angle = Angle;
			this.PieceIndex = PieceIndex;
			this.InPieceDistance = InPieceDistance;
			this.StartLaneIndex = StartLaneIndex;
			this.EndLaneIndex = EndLaneIndex;
			this.Speed = speed;
			this.Accel = accel;
			this.AngleSpeed = AngleSpeed;
			this.Lap = Lap;
		}
	}

	public Dictionary<Car.CarId, Car> Cars;

	public CarManager(JArray Data)
	{
		Cars = new Dictionary<Car.CarId, Car> ();
		foreach (var carInfo in Data)
		{
			Car car = new Car ((JObject)carInfo);
			Cars [car.Id] = car;
		}
	}

	public void Update(JArray Data)
	{
		foreach (var carInfo in Data)
		{
			Cars [new Car.CarId ((JObject)carInfo ["id"])].Update ((JObject)carInfo);
		}
	}
}


public class TrackInfo
{
	public class Piece
	{
		public enum PieceType
		{
			Straight,
			Bend
		}
		public double Length { get; private set; }

		public double Radius { get; private set; }
		public double Angle { get; private set; }

		public bool IsSwitch { get; private set; }
		public bool IsBridge { get; private set; }

		public PieceType Type { get; private set; }

		public Piece(JObject Data)
		{
			if (Data["length"] != null)
			{
				Length = (double)Data["length"];
				Type = PieceType.Straight;
			}
			else
			{
				Radius = (double)Data["radius"];
				Angle = (double)Data["angle"];
				Type = PieceType.Bend;
			}

			IsSwitch = Data["switch"] != null ? (bool)Data["switch"] : false;
			IsBridge = Data["bridge"] != null ? (bool)Data["bridge"] : false;
		}

		public double GetLength(long laneIndex)
		{
			if (this.Type == PieceType.Straight)
				return Length;

			double Angle = this.Angle > 0 ? this.Angle : -this.Angle;

			return GetRadius(laneIndex) * (Angle / 180.0) * Math.PI;
		}

		public double GetRadius(long laneIndex)
		{
			if (Type == PieceType.Straight)
				return double.MaxValue;
			double Angle = this.Angle;
			double Radius = this.Radius;
			if (Angle > 0) // right
			{
				Radius -= Bot.Track.Lanes [laneIndex];
			}
			else
			{
				Radius += Bot.Track.Lanes [laneIndex];
			}

			return Radius;
		}

		public double MaxSpeed(long laneIndex)
		{
			if (this.Type == PieceType.Bend)
				return Math.Sqrt(Bot.wheelFriction * GetRadius(laneIndex));
			return double.MaxValue;
		}
	}

	public string Name { get; private set; }
	public string Id { get; private set; }

	public Piece[] Pieces { get; private set; }
	public Dictionary<long, double> Lanes { get; private set; }

	//public double StartingPoint { get; private set; }

	public TrackInfo(JObject track)
	{
		JArray pieces = (JArray)track ["pieces"];

		this.Name = (string)track["name"];
		this.Id = (string)track["id"];

		Pieces = new Piece[pieces.Count];
		int i = 0;
		foreach (var p in pieces)
			Pieces [i++] = new Piece ((JObject)p);

		Lanes = new Dictionary<long, double> ();
		foreach (var l in (JArray)track["lanes"]) {
			Lanes [(int)l ["index"]] = (double)l ["distanceFromCenter"];
		}
	}
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		if (value > 1.0)
			value = 1.0;
		else if (value < 0.0)
			value = 0.0;
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}
